var urltemplate = location.origin + '/wp-content/themes/despierta_en_valladolid/img/';
(function( $ ){

	$(document).ready(function(){
		$('#header').owlCarousel({
			loop:true,
			margin:10,
			nav:false,
			navText:false,
			dots : true,
			items:1
		});

		$('.otros').owlCarousel({
			loop:true,
			autoplay:true,
			autoplayTimeout:2000,
			autoplayHoverPause:true,
			nav:false,
			navText:false,
			dots : false,
			items : 1 
		});

		$('.ebooks-item').owlCarousel({
			loop:false,
			margin:5,
			nav:false,
			navText:false,
			dots : false,
			responsiveClass:true,
		    responsive:{
		        0:{items:1},
		        768:{items:2},
		        1000:{items:2}
		    } 
		});
		function goTo(arg){
			var busqueda = "http://blog.valladolid.mx/?s=";
     		location.href = busqueda + arg;
		}
		$('#buscar').keypress(function(){        	
        	if ( event.which == 13 ) {
     			event.preventDefault();
     			goTo( $(this).val() );     			
  			}        	
    	});
    	$('#lupa').click(function(){
    		goTo( $('#buscar').val() );
    	});

    	$('.relacionados').owlCarousel({
			loop:false,
			margin:5,
			nav:true,
			navText:['<img class="izqs" src="'+urltemplate+'arrow.png">','<img class="dere" src="'+urltemplate+'arrow.png">'],
			dots: false,			
			responsive : {
		        0:{items:1},
		        550:{items:2},
		        1000:{items:3}
		    }
		});
	});
	
})(jQuery);