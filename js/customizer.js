jQuery(document).ready(function() {
    
    var formfield;
    /* user clicks button on custom field, runs below code that opens new window */
    var custom_uploader;
    var archivo = [];
    var currentImages;
    
    jQuery('.onetarek-upload-button').click(function(e) {
        
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Seleccionar archivo',
            button: {
                text: 'Seleccione su archivo pdf'
            },
            multiple: true
        });

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            
            attachment = custom_uploader.state().get('selection').toJSON();
            // var value = jQuery('#archivo').val();
            // if(value != ""){
            //     currentImages = JSON.parse(value);
            //     archivo = currentImages;
            // }
            // Comprobamos que sea un archivo pdf
            if(attachment[0].subtype == 'pdf')
            {
                archivo.push({
                    autor :attachment[0].authorName,
                    nombre:attachment[0].name,
                    url   :attachment[0].url,
                    icono :attachment[0].icon
                });
                jQuery('.content-archivo').remove();
                jQuery('.img_gal').prepend(
                        '<div class="content-archivo">'+
                        '<img src="'+archivo[0].icono+'">'+
                        '<a class="destroy-item">x</a>'+
                        '<span>'+ archivo[0].nombre +'</span></div>'
                    );/* fin del html() */
                jQuery('#archivo').val(JSON.stringify(archivo));       
            }else{
                alert('El archivo no es un pdf');
            }

        });

        //Open the uploader dialog
        custom_uploader.open();

    });
    jQuery( ".img_gal" ).on(
        "click", 
        ".destroy-item", 
        function() {
            if( confirm("¿Esta seguro que desea eliminar esta imagen?") )
            { 
                jQuery(this).parent().remove();
                jQuery('#archivo').val('');
            } 
        });

    /*
     Please keep these line to use this code snipet in your project
     Developed by oneTarek http://onetarek.com
     */
    //adding my custom function with Thick box close function tb_close() .
    window.old_tb_remove = window.tb_remove;
    window.tb_remove = function() {
        window.old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
        formfield=null;
    };

    // user inserts file into post. only run custom if user started process using the above process
    // window.send_to_editor(html) is how wp would normally handle the received data

    window.original_send_to_editor = window.send_to_editor;
    window.send_to_editor = function(html){
        if (formfield) {
            fileurl = jQuery('img',html).attr('src');
            jQuery(formfield).val(fileurl);
            tb_remove();
        } else {
            window.original_send_to_editor(html);
        }
    };
});

