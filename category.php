<?php get_header() ?>
<?php
$paged = (get_query_var('page')) ? get_query_var('page') : 1;
$cats = new WP_Query(array('posts_type'=>'post', 
							'category_name'=> single_cat_title("", false),
							'posts_per_page' => 6,
                            'paged' => $paged,
							'orderby'=>'date',
							'order' => 'DESC',
							)
					);
?>
<div id="home" class="row">
	<div class="columns large-9 medium-8 small-12 <?=$empty?>">
		<ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-2">			
			<?php 
				if( $cats->have_posts() ):
					echo '<h3 style="padding-left:10px;">Artículos encontrados para la categoría: '.single_cat_title("", false).'</h3><br>';
					while( $cats->have_posts() ) : 
						$cats->the_post(); 
						get_template_part( 'my-templates/tpl-post', get_post_format() );
					endwhile;
				else:
					echo '<h2>Aún no hay artículos de esta categoría</h2>';
				endif;
			?>
		</ul>
		<div class="paginate"><?php get_pagination($cats); ?></div>
	</div>
	<div class="columns large-3 medium-4 small-12">
		<?php include "sidebar2.php"; ?>
	</div>
	<div class="columns large-12 medium-12 small-12">
		<?php require get_template_directory() . '/my-templates/part-ebook.php'; ?>
	</div>
</div>
<?php get_footer() ?>