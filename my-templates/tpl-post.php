<li class="post prev-post">
	<?php 
		$img = has_post_thumbnail() ? 	    
	    wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) 
	    :'';
	?>
	<a href="<?php the_permalink() ?>">
		<div class="destacada" style="background:url(<?=$img?>)"></div>
		<h5><?php the_title() ?></h5>
	</a>
	<p><?=substr(strip_tags(get_the_content()), 0, 350)?></p>
	<div class="columns large-12 medium-12 small-12 botones">
		<div class="columns large-3 medium-6 child-botones">
			<div class="date"><img class="clock" src="<?=get_template_directory_uri()?>/img/clock.png"><?=the_time('j-M-Y')?></div><br>
		</div>
		<div class="columns large-2 medium-6 small-12 child-botones com">
			<a href="<?php the_permalink() ?>/#comentarios" class="btn-red coments">...
				<div>Ver comentarios</div>
				<span class="numcoments"><?=get_comments_number( $post->ID )?></span>
			</a><br><br>			
		</div>
		<div class="columns large-5 medium-7 child-botones">
			<span>Compartir en:</span>
			<?php j_share() ?>
		</div>
		<div class="columns large-3 medium-5 child-botones">
			<a href="<?php the_permalink() ?>" class="button right">Leer más</a>
		</div>
	</div>
</li>