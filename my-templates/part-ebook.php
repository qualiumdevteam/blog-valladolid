<div class="columns large-9 medium-12 small-12 ebooks" style="padding:0;">
		<h3 class="titulo">EBOOKS</h3>
	   	<div class="ebooks-item">
			<?php 
				wp_reset_query(); 
				$post='';
				$ebook = new WP_Query(array('post_type'=>'ebooks')); 
				
				while( $ebook->have_posts() ) : $ebook->the_post(); 
			
					$img = has_post_thumbnail() ? 	    
		    		wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) 
		    		:'';
		   	?>
				<div class="e-items">
					<a href="<?php the_permalink() ?>">
					<div class="img" style="background:url(<?=$img?>)">
						<div class="overlay"></div>
						<div class="letras">
							<h3>EBOOK</h3>
							<h3><?php the_title() ?></h3>	
						</div>			
						<span>+</span>
					</div> 		
					</a>
				</div>
			<?php endwhile; wp_reset_query();?>
		</div> <!--ebooks-item-->
</div>