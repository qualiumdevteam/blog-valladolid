<?php 
/*
 Title: Ebooks
 Post Type: post
 */
piklist('field', array(
    'type' => 'file'
    ,'field' => 'ebooks'
    ,'scope' => 'post_meta'
    ,'label' => __('Agregar ebook(s)','piklist')
    ,'description' => __('Carga el ebook pulsando el botón Añadir','piklist')
    ,'options' => array(
      'modal_title' => __('Añadir Ebook','piklist')
      ,'button' => __('Añadir','piklist')
    )
));