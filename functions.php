<?php
/**
 * despierta_en_valladolid functions and definitions
 *
 * @package despierta_en_valladolid
 */

if ( ! function_exists( 'despierta_en_valladolid_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function despierta_en_valladolid_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on despierta_en_valladolid, use a find and replace
	 * to change 'despierta_en_valladolid' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'despierta_en_valladolid', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'despierta_en_valladolid' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'despierta_en_valladolid_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // despierta_en_valladolid_setup
add_action( 'after_setup_theme', 'despierta_en_valladolid_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function despierta_en_valladolid_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'despierta_en_valladolid_content_width', 640 );
}
add_action( 'after_setup_theme', 'despierta_en_valladolid_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function despierta_en_valladolid_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'despierta_en_valladolid' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'despierta_en_valladolid_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function despierta_en_valladolid_scripts() {
	wp_enqueue_style( 'despierta_en_valladolid-style', get_stylesheet_uri() );

	wp_enqueue_script( 'despierta_en_valladolid-navigation', get_template_directory_uri() . '/libs/js/foundation.min.js', array('jquery'));
	wp_enqueue_script( 'owlcarousel', get_template_directory_uri() . '/libs/carousel/owl.carousel.min.js');
	wp_enqueue_style('owlcarouselcss', get_template_directory_uri() . '/libs/carousel/owl.carousel.css');
	wp_enqueue_style('fonts', get_template_directory_uri() . '/fontlato/stylesheet.css');
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js');
	wp_enqueue_script( 'momentjs', get_template_directory_uri() . '/js/moment.js');
	wp_enqueue_style('foundationcss', get_template_directory_uri() . '/libs/css/foundation.min.css');
	wp_enqueue_style( 'icons', get_template_directory_uri() .'/icons/flaticon.css' );
	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'despierta_en_valladolid_scripts' );

function j_share(){
	if ( function_exists( 'sharing_display' ) ) 
   	{
    	echo sharing_display( '', false );
	}
}

function get_pagination($query){
    $big = 999999999; // need an unlikely integer
    $page_current = 1;
    if(isset($_GET['page'])){
    	// $page_current = max( $_GET['paged'], get_query_var('paged') );
    	if(is_numeric($_GET['page']) && $_GET['page'] > 0){
    		$page_current = $_GET['page'];	
    	}    	
    }
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'base' => @add_query_arg('page','%#%'),
        'format' => '?paged=%#%',
        'prev_text'    => __('« Anterior '),
        'current' => $page_current,
        'next_text'    => __('Siguiente »'),
        'total' => $query->max_num_pages,
    ) );
}

function post_relacionados($catid) {

        global $post;

        $tag_ids = array();

        $current_cat = get_the_category($post->ID);
        $current_cat = $current_cat[0]->cat_ID;
        $this_cat = '';

        $tags = get_the_tags($post->ID);

        if ( $tags ){

            foreach($tags as $tag) {
                $tag_ids[] = $tag->term_id;
            }
        }else{
            $this_cat = $current_cat;
        }
        $args = array(
                'post_type'   => get_post_type(),
                'numberposts' => 8,
                'orderby'     => 'rand',
                'tag__in'     => $tag_ids,
                'cat'         => $catid,                
                'exclude'     => $post->ID             
        );

        $related_posts = get_posts($args);

        /**
         * If the tags are only assigned to this post try getting
         * the posts again without the tag__in arg and set the cat
         * arg to this category.
         */
        if ( empty($related_posts) ) {
                $args['tag__in'] = '';
                $args['cat'] = $current_cat;
                $related_posts = get_posts($args);
        }

        if ( empty($related_posts) ) {
                return;
        }

     
        foreach($related_posts as $related) {
        	echo "<div class='items-relacionados'><a href=".get_permalink($related->ID).">";
        	echo "<div class='relacionado text-center' style='background:url(".wp_get_attachment_url( get_post_thumbnail_id( $related->ID ) ).")'></div>";
        	echo "<h5 class='entry-title'>".$related->post_title."</h5></a></div>";        	
        }
        //echo $permalink."<h5 class='entry-title text-center'>".$related->post_title."</h5></a>";
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/post_type.php';