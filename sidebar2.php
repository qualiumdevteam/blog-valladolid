<?php
/**
 * The sidebar containing the main widget area.
 * dynamic_sidebar( 'sidebar-1' ); 
 * @package despierta_en_valladolid
 */
$url = get_template_directory_uri(); 
// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
// 	return;
// }
?>

<div id="secondary" class="widget-area" role="complementary">
	<br class="show-for-small-only">
	<h3>¡SUÍGUENOS!</h3>
	<div class="clearfix text-center">
		<a href="https://www.facebook.com/valladolidmx?fref=ts" target="_blank"><div class="misredes f"> <i class="flaticon-facebook23"></i>	</div></a>
		<a href="https://twitter.com/valladolidmx" target="_blank"><div class="misredes t"> <i class="flaticon-twitter1"></i>		</div></a>
		<a href="https://instagram.com/valladolidmx/" target="_blank"><div class="misredes i"> <i class="flaticon-instagram10"></i>	</div></a>
	</div>
	<div class="busqueda">
		<i class="flaticon-magnifying-glass32 lupa"></i>
		<input type="search" id="buscar" name="s" value="">					
	</div>

<div class="listado_categorias">
	<h3 class="categorias">CATEGORÍAS</h3>
	<ul class="no-bullet">
	<?php 
		$args = array (
			'orderby'    => 'count',
			'hide_empty' => 0,
		 	'title_li' => false,
		 	'style' => 'list',
		 	'show_count' => 1
		);

 	wp_list_categories($args);
 wp_reset_query();
 $query = new WP_Query(array('post_type'=>'banner'));
 ?>
	</ul>
</div>
	<div class="otros columns">
	<?php 
		while( $query->have_posts() ) : 
			$query->the_post(); 
			$img = has_post_thumbnail() ? 	    
	    		wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) 
	    		:'';
	?>
		<a href="<?php echo get_the_content() ?>" target="_blank">
			<div class="item" style="background:url(<?=$img?>)">
				<?php the_title('<h5>','</h5>');?>
			</div>		
		</a>
	<?php endwhile;wp_reset_query();  ?>
	</div>
</div><!-- #secondary -->
