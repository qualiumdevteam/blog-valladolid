<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package despierta_en_valladolid
 */

get_header(); ?>

	<div id="primary" class="row">
		<section class="error-404 not-found columns large-9 medium-9 small-12">
			<header class="page-header">
				<h1 class="page-title"><?php esc_html_e( 'No ha sido encontrado ese término.', 'despierta_en_valladolid' ); ?></h1>
			</header><!-- .page-header -->

			<div class="page-content">
				<?php //get_search_form(); ?>
					<?php if ( despierta_en_valladolid_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
				<div class="widget widget_categories">
					<h2 class="widget-title"><?php esc_html_e( 'Categorías mas utilizadas', 'despierta_en_valladolid' ); ?></h2>
						<ul>
					<?php
						wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
				</div><!-- .widget -->
				<?php endif; ?>

				<?php
					/* translators: %1$s: smiley */
					//$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'despierta_en_valladolid' ), convert_smilies( ':)' ) ) . '</p>';
					//the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
				?>

				<?php //the_widget( 'WP_Widget_Tag_Cloud' ); ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
			<div class="columns large-3 medium-3 small-12">
				<?php get_sidebar() ?>
			</div>
		<!--</main> #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
