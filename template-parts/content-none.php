<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package despierta_en_valladolid
 */

?>

	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'No se encontró', 'despierta_en_valladolid' ); ?></h1>
	</header><!-- .page-header -->
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
		<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'despierta_en_valladolid' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
	<?php elseif ( is_search() ) : ?>
		<p><?php esc_html_e( 'Disculpe no se ha encontrado nada con este término, intente nuevamente con otra palabra.', 'despierta_en_valladolid' ); ?></p>
	<?php else : ?>
		<p><?php //esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'despierta_en_valladolid' ); ?></p>
	<?php endif; ?>
