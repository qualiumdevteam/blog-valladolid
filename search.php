<?php
/**
 * The template for displaying search results pages.
 *
 * @package despierta_en_valladolid
 */
$query = new WP_Query(array( 'post_type' => 'post', 's' => get_query_var('s')));
get_header(); ?>

	<section id="primary" class="row" style="margin-top:20px;">
		<div class="columns large-9 medium-9 small-12">
			<?php if ( $query->have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title" style="margin-bottom:20px;"><?php printf( esc_html__( 'Resultados para: %s', 'despierta_en_valladolid' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->
			<ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-2">
				<?php /* Start the Loop */ ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>

					<?php
					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
						get_template_part( 'my-templates/tpl-post', get_post_format() );
					// get_template_part( 'template-parts/content', 'search' );
					?>

				<?php endwhile; ?>
			</ul>
		
		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>
		</div>
		<div class="columns large-3 medium-3 small-12">
			<?php get_sidebar() ?>
		</div>
	</section><!-- #primary -->
	<div class="columns large-12">
			<?php the_posts_navigation(); ?>
	</div>
<?php get_footer(); ?>
