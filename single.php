<?php
/**
 * The template for displaying all single posts.
 *
 * @package despierta_en_valladolid
 */

get_header(); 
$catpost=0;
?>
<style>
@media only screen and (min-width: 640px) and (max-width: 1024px) {
    .listado_categorias{
    	    width: 50%;
    	    float: left;
            padding-left: 0.9375rem;
            padding-right: 0.9375rem;
	}
	.otros{
	    width: 50%;
    	    float: left;
            padding-left: 0.9375rem;
            padding-right: 0.9375rem;
	}
}
</style>
<div id="primary" class="row" style="padding-top:40px">
<div class="columns large-9 medium-12 small-12 post">
<?php while ( have_posts() ) : the_post(); ?>
<?php 
$catpost = get_cat_ID( get_the_category ( get_the_ID() ) );
?>
<?php get_template_part( 'template-parts/content', 'single' ); ?>

<?php //the_post_navigation(); ?>
<?php endwhile; // End of the loop. ?>
<div id="comentarios">
<?php
// If comments are open or we have at least one comment, load up the comment template.
if ( comments_open() || get_comments_number() ) :
comments_template();
endif;
?>
</div>
</div>
<div class="columns large-3 medium-12 small-12 end">
<?php get_sidebar(); ?>
</div>
<div class="clearfix"></div>
<div class="columns large-9 medium-12 small-12 end"><br>
<h4 class="text-left title-relacionados">Artículos Relacionados</h5>
<div class="relacionados">
<?php post_relacionados($catpost) ?>
</div>
</div>
<!--</main> #main -->
</div><!-- #primary -->
<?php get_footer(); ?>