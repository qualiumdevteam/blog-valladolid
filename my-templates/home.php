<?php 
/*
*	Template Name: home
*/
get_header();
$url = get_template_directory_uri();
$paged = (get_query_var('page')) ? get_query_var('page') : 1;
$home = new WP_Query(array('posts_type'=>'post', 
							// 'category_name'=>'blog',
							'posts_per_page' => 6,
                            'paged' => $paged,
							'orderby'=>'date',
							'order' => 'DESC',
							)
					);
?>
<div id="home" class="row">
	<div class="columns large-9 medium-12 small-12">
		<ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-2">
			<?php 
				while( $home->have_posts() ) : 
					$home->the_post(); 
					get_template_part( 'my-templates/tpl-post', get_post_format() );
				endwhile;
			?>
		</ul>
        <div class="paginate"><?php get_pagination($home) ?></div>
	</div>
	<div class="columns large-3 medium-12 small-12 sidebar">
		<?php get_sidebar();?>
	</div>
    <div class="clearfix"></div>
	<div class="columns large-12 medium-12 small-12">
		<?php include "part-ebook.php"; ?>
	</div>
</div>
<?php get_footer()?>