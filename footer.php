<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package despierta_en_valladolid
 */

?>
<div class="clearfix text-center prefoter">
	<h3>SÍGUENOS</h3>
	<div class="iconos">
		<a href="https://www.facebook.com/valladolidmx?fref=ts" target="_blank"><i class="facebook flaticon-facebook23"></i></a>
		<a href="https://twitter.com/valladolidmx" target="_blank"><i class="twitter flaticon-twitter1"></i>	</a>
		<a href="https://instagram.com/valladolidmx/" target="_blank"><i class="instagram flaticon-instagram10"></i></a>
		<!-- <a href=""><i class="black flaticon-black218"></i>	</a> -->
	</div>
</div>
<footer class="footer">
	<p>Copyright © Despierta en Valladolid. Todos los derechos reservados.</p>
</footer>
<?php wp_footer(); ?>

</body>
</html>
