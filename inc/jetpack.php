<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package despierta_en_valladolid
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function despierta_en_valladolid_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'despierta_en_valladolid_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function despierta_en_valladolid_jetpack_setup
add_action( 'after_setup_theme', 'despierta_en_valladolid_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function despierta_en_valladolid_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function despierta_en_valladolid_infinite_scroll_render
