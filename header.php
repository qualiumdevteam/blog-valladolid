<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package despierta_en_valladolid
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<?php $url = get_template_directory_uri(); ?>
<body <?php body_class(); ?>>
<div class="content-header">
<img class="leftPleca" src="<?=$url?>/img/leftFranja.png">
<img class="rightPleca" src="<?=$url?>/img/rightFranja.png">
	<div id="header" class="container">
		<div class="item" style="background:url(<?=$url?>/img/bg01.jpg)">
			<div class="ove"></div>
		</div>
		<div class="item" style="background:url(<?=$url?>/img/bg02.jpg)">
			<div class="ove"></div>
		</div>
	</div>
	<div class="forms">
		<div class="row">
			<div class="columns large-6 medium-6 small-6">
				<a href="<?=home_url()?>">
					<img src="<?=$url?>/img/valladolidlogo.png">
				</a>
				<h4 class="download">Descubre el Oriente <br> Maya de Yucatán</h4>
			</div>
			<div class="suscribir columns large-4 large-offset-2 medium-6 small-6">
				<h4>Obtén GRATIS la guía<br>turistica de Valladolid</h4>
				<?php get_template_part( 'my-templates/form-suscripcion', get_post_format() ); ?>
			</div>
		</div>
	</div>
</div>

	
