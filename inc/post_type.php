<?php
add_action( 'init', 'create_post_type' );
function create_post_type()
{
    register_post_type('Banner',
        array(
            'labels' => array(
                'name' => __('Banner'),
                'singular_name' => __('banner')                
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title','editor','thumbnail')
        )
    );
    register_post_type('Ebooks',
        array(
            'labels' => array(
                'name' => __('Ebooks'),
                'singular_name' => __('ebook')                
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title','editor','thumbnail','comments')
        )
    );
}
// 
// function get_amount_downloads_ebooks( $post_ID ){
    
//     $amount = get_post_meta( $post_ID , 'amount_downloads', true );
//     return (!empty($amount)) ? $amount : 0;
// }

add_action('admin_print_scripts', 'wp_fancy_commerce_admin_scripts');
function wp_fancy_commerce_admin_scripts(){
    wp_register_script('my-upload', get_template_directory_uri() .'/js/customizer.js', array('jquery','media-upload','thickbox','wp-color-picker'));
    wp_enqueue_script('my-upload');
    wp_enqueue_script('jquery');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script( 'common' );    
    // Estilos para el modulo entradas del administrador
    wp_enqueue_style( 'modulo-ebooks', get_template_directory_uri() .'/css/modulo-ebooks.css' ); 
}

add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes() {    
    add_meta_box( 
        'cyb-meta-box_filebook'
        ,__('Archivos pdf')
        ,'cyb_meta_box_filebook'
        ,'ebooks' 
    );
    // guarda la cantidad de descargas de cada ebook
    // add_meta_box( 
    //     'cyb-meta-box_amount_downloads'
    //     ,__('Descargas')
    //     ,'cyb_meta_box_amount_downloads'
    //     ,'ebooks' 
    // );
}

function cyb_meta_box_filebook(){
    global $post;
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    
    $file = json_decode( get_post_meta($post->ID,'filebook',true) );
    
    $html .= '<div class="img_gal">';
    if( is_array($file) ){

        $html .= '<div class="content-archivo"><img src="'.$file[0]->icono.'"><a class="destroy-item">x</a><span>'.$file[0]->nombre.'</span></div>';
    }
    $html .= "<input id='archivo' type='hidden' name='filebook' value='".get_post_meta($post->ID,'filebook',true)."' size='50'>";
    $html .= '<input type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar archivo" />';
    $html .= '</div>';
    echo $html;
}
// ADD NEW COLUMN
function ST4_columns_head($defaults) {
    $defaults['descargas'] = 'Descargas';
    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function ST3_columns_content($column_name, $post_ID) {
    $amount = 0;
    if ($column_name == 'descargas') {
        $amount = get_post_meta( $post_ID , 'descargas', true );        
    }
    echo $amount;//"<span>$amount</span></td>";
}

add_filter('manage_posts_columns', 'ST4_columns_head');
add_action('manage_posts_custom_column', 'ST3_columns_content', 16, 2);

// function amount_downloads(){
//     global $post;
//     wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    
//     get_post_meta($post->ID,'amount_downloads',true) );
    
//     $html .= '<div class="img_gal"><label>'. .'</label></div>';

// }
add_action( 'save_post', 'dynamic_save_postdata' );
function dynamic_save_postdata( $post_id ) {

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['dynamicMeta_noncename2'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename2'], plugin_basename( __FILE__ ) ) )
        return;

    if(isset($_POST['filebook'])){    
        update_post_meta($post_id,'filebook',$_POST['filebook']);
    }
}



// function generic_field( $type, $elid, $field, $placeholder ){
//     $clave = get_post_meta( $elid , $field, true );
//     $clave = (!empty($clave)) ? $clave : '';

//     wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
//     $html = '<div class="myfields">';
//     $html .= "<input type='".$type."' name='".$field."' value='".$clave."' placeholder='".$placeholder."'>";
//     $html .= '</div>';
//     echo $html;
// }