<?php
/**
 * Template part for displaying single posts.
 *
 * @package despierta_en_valladolid
 */
?>
<article id="post-<?php the_ID(); ?>" <?php //post_class('columns large-9 medium-12 small-12 post'); ?>>
	<?php 		
		$img = has_post_thumbnail() ? 	    
	    wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) 
	    :'';
	?>
	<img width="100%" data-interchange="[<?=$img?>, (small)], [<?=$img?>, (medium)], [<?=$img?>, (large)]" data-uuid="interchange-ibe9hh810" src="<?=$img?>">
	<div class="entry-content">
		<h1><?php the_title(); ?></h1>
		<?php if( get_post_meta( $post->ID, 'filebook', true) ): ?>
			<?php $file = json_decode(get_post_meta( $post->ID, 'filebook', true)); ?>
			<p>
				<a href="<?=$file[0]->url?>" download="<?=$file[0]->nombre?>">
					Descarga el Ebook <?=$file[0]->nombre?> Aquí
				</a>			
			</p>
		<?php endif;?>
		<?php the_content() ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->