<?php get_header(); ?>
	<div id="primary" class="row" style="padding-top:40px">
	<div class="columns large-9 medium-12 small-12">
		<?php 
			while ( have_posts() ) : 
				$catpost = get_cat_ID( get_the_category ( get_the_ID() ) );
				the_post(); 
				get_template_part( 'template-parts/content', 'single' );
			endwhile; 
		?>
		</div>
		<div class="columns large-3 medium-12 small-12">
			<?php get_sidebar(); ?>
		</div>
		<div class="clearfix"></div>
		<div id="comentarios" class="columns large-9 medium-12 small-12 end"><br>
			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>
		</div>
		<div class="columns large-9 medium-12 small-12 end"><br>
			<h4 class="text-left title-relacionados">Otros Ebooks</h5>
			<div class="relacionados">
				<?php post_relacionados($catpost) ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>